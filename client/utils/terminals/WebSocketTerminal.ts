import { Terminal } from 'xterm'
import { ITerminalOperations } from '../types'

export class WebSocketTerminal implements ITerminalOperations {
  handleLineFeed(line: string): void {
    console.log(line)
  }

  establishConnection(): void {
  }

  sendMessage(message: string): void {
      console.log(message)
  }

  initTerminal(term: Terminal): void {
        term.write("Websocket Init")
  }
}
