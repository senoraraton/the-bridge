import { Terminal } from "xterm";
import { ITerminalOperations } from "../types";

type LoginDetails = {
  username?: string;
  password?: string;
};

const backendUrl = process.env.BACKEND_URL || "http://localhost:9091";

export class DummyTerminal implements ITerminalOperations {
  private mainState: "Register" | "Login" | "Tour" | "Default" = "Default";
  private registerState:
    | "Username"
    | "Password"
    | "Email"
    | "Submitting"
    | "Default" = "Default";
  private loginState:
    | "Username"
    | "Password"
    | "Register"
    | "Submitting"
    | "Default" = "Default";

  private loginDetails: LoginDetails = {};

  handleLineFeed(line: string, prompt: string, term: Terminal): void {
    line = line.toLowerCase();
    switch (this.mainState) {
      case "Register":
        this.handleRegister(line, term);
        break;
      case "Login":
        this.handleLogin(line, term);
        break;
      case "Tour":
        this.beginTour(line, term);
        break;
      case "Default":
        this.handleDefaultState(line, term);
        break;
      default:
        this.setDefaultState(term, prompt);
    }
  }

  public setDefaultState(term: Terminal, prompt: string): void {
    this.mainState = "Default";
    term.reset();
    term.write(
      "Please fasten your seatbelts and return your tray table to its full upright and locked position.\r\n",
    );
    term.write("Please select an option from the following list:\r\n");
    term.write("1) Register\r\n");
    term.write("2) Login\r\n");
    term.write("3) Tour\r\n");
    term.write("4) About Me\r\n");
    term.write(prompt);
  }

  private handleDefaultState(line: string, term: Terminal) {
    switch (line) {
      case "register":
      case "1":
        this.mainState = "Register";
        this.handleRegister(line, term);
        break;
      case "login":
      case "2":
        this.mainState = "Login";
        this.handleLogin(line, term);
        break;
      case "tour":
      case "3":
        this.mainState = "Tour";
        this.beginTour(line, term);
        break;
      case "about me":
      case "4":
        this.aboutMe(term);
        break;
      default:
        console.log(
          "Please try again, a message brought to you by your friends at Yoplait.",
        );
        break;
    }
  }

  private async handleRegister(line: string, term: Terminal): Promise<void> {
    switch (this.registerState) {
      case "Default":
        term.write("Please enter your desired username:");
        this.registerState = "Username";
        break;
      case "Username":
        console.log("Username entered as:", line);
        this.loginDetails.username = line;
        term.write("Please enter your password:");
        this.registerState = "Password";
        break;
      case "Password":
        console.log("Password entered as:", line);
        this.loginDetails.password = line;
        term.write("Please enter your email:");
        this.registerState = "Email";
        break;
      case "Email":
        console.log("Email entered as:", line);
        term.write("Submitting Registration...\n");
        this.registerState = "Submitting";
        await this.submitRegister(this.loginDetails);
        this.loginState = "Submitting";
        term.write("Logging in.");
        await this.submitLogin(this.loginDetails);
        break;
      case "Submitting":
        term.write("Please hold, your request is very important to us.");
        break;
    }
  }

  private async handleLogin(line: string, term: Terminal): Promise<void> {
    switch (this.loginState) {
      case "Default":
        term.write("Please enter your login:");
        this.loginState = "Username";
        break;
      case "Username":
        term.write("Please enter your password:");
        this.loginDetails.username = line;
        this.loginState = "Password";
        break;
      case "Password":
        term.write("Logging in...");
        this.loginDetails.password = line;
        await this.submitLogin(this.loginDetails);
        console.log("Login Successful.");
        break;
    }
  }

  private async submitRegister(loginDetails: LoginDetails): Promise<void> {
    const endpoint = backendUrl + "/register";
    try {
      const response = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(loginDetails),
      });

      if (response.ok) {
        console.log("Reg success");
      } else {
        console.error("Reg Failed:", response.statusText);
      }
    } catch (error) {
      console.error("Error during reg:", error);
    }
  }

  private async submitLogin(loginDetails: LoginDetails): Promise<void> {
    const endpoint = backendUrl + "/login";
    try {
      const response = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(loginDetails),
      });

      if (response.ok) {
        console.log("Login success");
      } else {
        console.error("Login Failed:", response.statusText);
      }
    } catch (error) {
      console.error("Error during login:", error);
    }
  }

  private beginTour(line: string, term: Terminal): void {
    term.clear();
    term.write("\b \b");
    term.write("toUr");
    console.log(line);
  }

  private aboutMe(term: Terminal): void {
    term.write("HTP!\n");
  }
}
