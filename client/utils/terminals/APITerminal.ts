import { Terminal } from 'xterm'
import { ITerminalOperations } from '../types'

export class APITerminal implements ITerminalOperations {
  handleLineFeed(line: string): void {
      console.log(line)
  }

  initTerminal(term: Terminal): void {
        term.write("API Init")
  }
}
