import { Terminal, ITerminalOptions } from "xterm";
import { TerminalExtensions, ITerminalOperations } from "./types";

export class TerminalAdapter {
  protected term: Terminal;
  protected termExtensions: TerminalExtensions;
  protected terminalOpts: ITerminalOptions;
  private terminalInstance: ITerminalOperations;

  constructor(
    terminalOpts: ITerminalOptions,
    termExtensions: TerminalExtensions,
    terminalInstance: ITerminalOperations,
  ) {
    this.term = new Terminal({ ...termExtensions, ...terminalOpts });
    this.termExtensions = termExtensions;
    this.terminalOpts = terminalOpts;
    this.termKeyCapture();
    this.terminalInstance = terminalInstance;
    this.terminalInstance.setDefaultState(
      this.term,
      this.termExtensions.prompt,
    );
    const containerElement = document.getElementById(this.termExtensions.div);
    if (containerElement) {
      this.term.open(containerElement);
    } else {
      console.error(
        "Div not found. Please ensure:" + containerElement + "exists.",
      );
    }
  }

  protected termKeyCapture(): void {
    let buffer: string = "";
    this.term.onKey((e: { key: string; domEvent: KeyboardEvent }) => {
      const ev = e.domEvent;
      const printable = !ev.altKey && !ev.ctrlKey && !ev.metaKey;

      if (ev.key === "Enter") {
        this.terminalInstance.handleLineFeed(
          buffer,
          this.termExtensions.prompt,
          this.term,
        );
        buffer = "";
        this.term.write("\r\n" + this.termExtensions.prompt);
      } else if (ev.key === "Backspace") {
        if (this.term.buffer.active.cursorX > 1) {
          this.term.write("\b \b");
        }
      } else if (printable) {
        this.term.write(e.key);
        buffer += e.key;
        console.log("Current Buffer:", buffer);
      }
    });
  }
  protected showBoldColorsInTerminal(term: Terminal) {
    const boldColors: string[] = [];

    for (let i = 30; i <= 37; i++) {
      boldColors.push(`\x1b[1;${i}mBOLD COLOR ${i}\x1b[0m`);
    }

    boldColors.forEach((color) => term.writeln(color));
  }
}
