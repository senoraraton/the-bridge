import { FitAddon } from "xterm-addon-fit";

export type TerminalExtensions = {
  div: string;
  charHeight: number;
  prompt: string;
  fitAddon: FitAddon | null;
};
