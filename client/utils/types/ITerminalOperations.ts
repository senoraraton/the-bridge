import { Terminal } from "xterm";

export interface ITerminalOperations {
  setDefaultState(term: Terminal, prompt: string): void;
  handleLineFeed(line: string, prompt: string, term: Terminal): void;
}
