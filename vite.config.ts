import path from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
      alias: {
        'common/Types': path.resolve(__dirname, 'node_modules/xterm/src/common/Types.d.ts'),
        'common/data/EscapeSequences': path.resolve(__dirname, 'node_modules/xterm/src/common/data/EscapeSequences.ts'),
        '@/utils': '/client/utils',
        '@/views': 'client/views',
        '@/components': '/client/components'
      },
  },
})
